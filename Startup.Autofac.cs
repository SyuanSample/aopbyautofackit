using System.Reflection;
using Autofac;
using Autofac.Extras.DynamicProxy;
using Castle.DynamicProxy;

namespace AopByAutofacKit
{
    public partial class Startup
    {
        public static void ConfigureDevelopmentContainer(ContainerBuilder builder)
        {
            //  註冊要使用的AOP
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                   .AssignableTo(typeof(IInterceptor));

            //  註冊要使用的interface
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                   .AsImplementedInterfaces()
                   .EnableInterfaceInterceptors();
        }
    }
}