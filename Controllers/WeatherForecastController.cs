using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AopByAutofacKit.Domain;
using AopByAutofacKit.Interface;

namespace AopByAutofacKit.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly ISample _sample;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, ISample sample)
        {
            _logger      = logger;
            _sample = sample;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var result = _sample.GetData();

            return result;
        }
    }
}
