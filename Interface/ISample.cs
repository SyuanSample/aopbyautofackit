using AopByAutofacKit.Domain;

namespace AopByAutofacKit.Interface
{
    public interface ISample
    {
        public WeatherForecast[] GetData();
    }
}