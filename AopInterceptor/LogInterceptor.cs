using System;
using System.Diagnostics;
using Castle.DynamicProxy;

namespace AopByAutofacKit.AopInterceptor
{
    public class LogInterceptor : IInterceptor
    {
        public LogInterceptor()
        {
        }
        /// <summary>
        ///     紀錄API執行的時間
        /// </summary>
        /// <param name="invocation"></param>
        public void Intercept(IInvocation invocation)
        {
            //  開始前要做甚麼的處理
            var sw = Stopwatch.StartNew();

            //執行方法
            invocation.Proceed();

            //  結束後要做甚麼處理
            var total = sw.Elapsed.ToString("G");
            Console.WriteLine(total);
        }
    }
}