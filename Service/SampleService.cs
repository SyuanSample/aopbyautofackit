using System;
using System.Linq;
using AopByAutofacKit.AopInterceptor;
using AopByAutofacKit.Domain;
using AopByAutofacKit.Interface;
using Autofac.Extras.DynamicProxy;

namespace AopByAutofacKit.Service
{
    [Intercept(typeof(LogInterceptor))]
    public class SampleService : ISample
    {
        private static readonly string[] Summaries =
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public WeatherForecast[] GetData()
        {
            var rng = new Random();

            var result = Enumerable.Range(1, 5)
                                   .Select
                                   (
                                       index => new WeatherForecast
                                       {
                                           Date    = DateTime.Now.AddDays(index), TemperatureC = rng.Next(-20, 55)
                                         , Summary = Summaries[rng.Next(Summaries.Length)]
                                       }
                                   )
                                   .ToArray();

            return result;
        }
    }
}