using AopByAutofacKit.Interface;
using AopByAutofacKit.Service;
using Microsoft.Extensions.DependencyInjection;

namespace AopByAutofacKit
{
    public partial class Startup
    {
        public static void RegisterService(IServiceCollection services)
        {
            services.AddScoped<ISample, SampleService>();
        }
    }
}